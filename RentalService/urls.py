"""RentalService URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import path
from flats.views import (FlatRetrieveUpdateDestroyView, FlatsListView,
                         RoomsListView, RoomsRetrieveView)
from order.views import OrderListCreateView, OrderRetrieveUpdateDestroyView
from rest_framework_simplejwt import views as jwt_views
from users.views import CustomUserCreate, CustomUserDetail, CustomUserList

from .views import api_root

urlpatterns = [
    # root
    path('', api_root),
    # admin
    path('admin/', admin.site.urls),
    # jwt
    path('api/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(),
         name='token_refresh'),
    # users
    path('api/v1/register/', CustomUserCreate.as_view(), name='register'),
    path('api/v1/users/', CustomUserList.as_view(), name='users'),
    path('api/v1/users/<int:pk>/', CustomUserDetail.as_view(), name='user'),
    # rooms
    path('api/v1/flats/<int:flat_pk>/rooms/', RoomsListView.as_view(), name='rooms'),
    path('api/v1/flats/<int:flat_pk>/rooms/<int:pk>/', RoomsRetrieveView.as_view(),
         name='room'),
    # flats
    path('api/v1/flats/', FlatsListView.as_view(), name='flats'),
    path('api/v1/flats/<int:pk>/', FlatRetrieveUpdateDestroyView.as_view(), name='flat'),
    # order
    path('api/v1/order/', OrderListCreateView.as_view(), name='orders'),
    path('api/v1/order/<int:pk>/', OrderRetrieveUpdateDestroyView.as_view(),
         name='order'),
]

if settings.DEBUG or settings.TESTING_MODE:
    import debug_toolbar
    from django.urls import include

    urlpatterns = [path('__debug__/', include(debug_toolbar.urls)), ] + urlpatterns
