from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse


@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'GET TOKEN': reverse('token_obtain_pair', request=request, format=format),
        'UPDATE TOKEN': reverse('token_refresh', request=request, format=format),
        'REGISTER NEW USER': reverse('register', request=request, format=format),
        'GET USERS INFO': reverse('users', request=request, format=format),
        'GET FLATS INFO': reverse('flats', request=request, format=format),
    })
