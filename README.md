# rental-service

Test task for python backend developer.
Description of the task [here](https://gitlab.com/vshagur/rental-service/-/wikis/home). 


### DOCUMENTATION

[Installation](https://gitlab.com/vshagur/rental-service/-/wikis/Installation)

[Testing](https://gitlab.com/vshagur/rental-service/-/wikis/Tests)

[Run](https://gitlab.com/vshagur/rental-service/-/wikis/Run)

[Usage](https://gitlab.com/vshagur/rental-service/-/wikis/Usage)

[API Description](https://gitlab.com/vshagur/rental-service/-/wikis/APIDescription)



#### BUILT WITH

[Django](https://www.djangoproject.com/)

[djangorestframework](https://www.django-rest-framework.org/)

[djangorestframework-simplejwt](https://github.com/jazzband/djangorestframework-simplejwt)

[psycopg2-binary](https://pypi.org/project/psycopg2-binary/)

[postgresql](https://www.postgresql.org/)

[Docker](https://www.docker.com/)


#### AUTHORS

Valeriy Shagur  - [vshagur](https://github.com/vshagur), email: vshagur@gmail.com

#### LICENSE

This project is licensed under the MIT License - see the [LICENSE](https://gitlab.com/vshagur/rental-service/-/blob/dev/LICENSE) file for details






