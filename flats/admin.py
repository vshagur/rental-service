from django.contrib import admin
from flats.models import (Build, Flat, FlatAttribute, FlatAttributesValue,
                          FlatRoom)

admin.site.register(Build)
admin.site.register(Flat)
admin.site.register(FlatRoom)
admin.site.register(FlatAttribute, )
admin.site.register(FlatAttributesValue)
