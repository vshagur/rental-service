from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from flats.filters import FlatFilter
from flats.models import Flat, FlatRoom
from flats.serializers import (BaseFlatsSerializer, FlatCreateSerializer,
                               FlatRoomSerializer, FlatUpdateSerializer)
from rest_framework import filters, generics, permissions, status
from rest_framework.exceptions import NotFound
from rest_framework.parsers import JSONParser
from rest_framework.response import Response


class FlatsListView(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Flat.objects.all()
    parser_classes = (JSONParser,)
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filterset_fields = ('build_id', 'type', 'price', 'created_at')
    search_fields = ('price',)
    ordering_fields = ('build_id', 'room_count', 'owner_id')
    filterset_class = FlatFilter

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return BaseFlatsSerializer
        return FlatCreateSerializer

    def post(self, request, *args, **kwargs):
        if request.user.status != 'owner':
            content = {'detail': 'Only users with the "owner" status can add flats.'}
            return Response(content, status=status.HTTP_403_FORBIDDEN)

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        content = serializer.save()

        return Response(content, status=status.HTTP_201_CREATED)


class FlatRetrieveUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Flat.objects.all()

    def get_serializer_class(self):
        if self.request.method in ('GET', 'DELETE'):
            return BaseFlatsSerializer
        else:
            return FlatUpdateSerializer

    def delete(self, request, *args, **kwargs):
        try:
            flat = Flat.objects.get(pk=kwargs.get('pk'))
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if request.user == flat.owner_id:
            flat.delete()
            content = {'detail': f'Flat id={flat.id} deleted!'}
            return Response(content, status=status.HTTP_204_NO_CONTENT)
        else:
            content = {'detail': f'The user is not the owner of the flat.'}
            return Response(content, status=status.HTTP_403_FORBIDDEN)


class RoomsRetrieveView(generics.RetrieveAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = FlatRoom.objects.all()
    serializer_class = FlatRoomSerializer

    def get_queryset(self):
        flat_pk = self.kwargs.get('flat_pk', None)

        try:
            flat = Flat.objects.get(id=flat_pk)
        except Flat.DoesNotExist:
            raise NotFound('A flat with this id does not exist')

        return self.queryset.filter(flat_id=flat)

    def retrieve(self, request, *args, **kwargs):
        room_id = self.kwargs.get('pk')
        queryset = self.get_queryset()
        room = get_object_or_404(queryset, pk=room_id)
        serializer = self.serializer_class(room)

        return Response(serializer.data)


class RoomsListView(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = FlatRoom.objects.all()
    serializer_class = FlatRoomSerializer

    def get_queryset(self):
        flat_pk = self.kwargs.get('flat_pk', None)

        try:
            flat = Flat.objects.get(id=flat_pk)
        except Flat.DoesNotExist:
            raise NotFound('A flat with this id does not exist')

        return self.queryset.filter(flat_id=flat)

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)
