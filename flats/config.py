from enum import Enum


class BaseTypes(Enum):
    """
    Base class for classes containing descriptions of type parameters.
    """

    @classmethod
    def get_types_list(cls):
        return [attr.value for attr in cls]

    @classmethod
    def get_types_choices(cls):
        return [(attr.value, attr.value.upper()) for attr in cls]


class FlatTypes(BaseTypes):
    """
    There are different types of houses.
    A cottage - a small house in the countryside
    A mansion -a large and especially beautiful house
    A manor - a large house with a lot of land and small other buildings for different
    purposes around.
    An apartment block/a block of flats- a high building with a lot of flats inside.
    A detached house - a house which isn’t joined to another house
    Semi-detached houses - houses which share one wall between them
    A row of similar houses joined together on both sides is called terraced houses.
    A flat - a set of rooms in a building
    A bungalow -a house on one level with a veranda.
    A lodge - a small house which is used when you’re fishing or hunting.
    A shed -a special building, where different things are kept
    A stable -a special building where horses are kept
    A greenhouse - a special building where plants are grown
    A council house belongs to a local town/city council, so a rent can be lower.
    A community centre - a special building for meetings, special events and other
    activities.
    """

    COTTAGE = 'cottage'
    MANSION = 'mansion'
    MANOR = 'manor'
    APARTMENT = 'apartment'
    HOUSE = 'house'
    SEMI_DETACHED_HOUSES = 'semi_detached_houses'
    TOWNHOUSE = 'townhouse'
    FLAT = 'flat'
    BUNGALOW = 'bungalow'
    LODGE = 'lodge'
    SHED = 'shed'
    STABLE = 'stable'
    GREENHOUSE = 'greenhouse'
    COUNCIL_HOUSE = 'council_house'
    COMMUNITY_CENTRE = 'community_centre'


class RoomTypes(BaseTypes):
    """
    Kitchen: Intended for the preparation (and perhaps also consumption) of food.
    (Some houses may include a pantry which is used for storing food.)
    Bedroom: Intended for sleeping, storing clothes, etc.
    Bathroom: Contains a bath and/or shower, and can include sanitary accommodation.
    Dining room: For communal eating and socializing.
    Living room/lounge: A social room for relaxation.
    Study: For work and administrative tasks.
    Laundry room: For washing and ironing laundry (can also be referred to as a utility
    room). Dwellings in Scotland may have a drying room.
    Toilet: Separate room containing a toilet and usually a sink.
    Shower room: Separate room containing a shower and sometimes a sink.
    Box room: A small room that may serve as a child’s bedroom, playroom, or storage room.
    """

    KITCHEN = 'Kitchen'
    BEDROOM = 'bedroom'
    BATHROOM = 'bathroom'
    DINING_ROOM = 'dining_room'
    LIVING_ROOM = 'living_room'
    STUDY = 'study'
    LAUNDRY_ROOM = 'laundry_room'
    TOILET = 'toilet'
    SHOWER_ROOM = 'shower_room'
    BOX_ROOM = 'box_room'
