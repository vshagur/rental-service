from django.core.exceptions import ObjectDoesNotExist
from flats.config import FlatTypes
from flats.models import (Build, Flat, FlatAttribute, FlatAttributesValue,
                          FlatRoom)
from rest_framework import serializers


class BuildSerializer(serializers.ModelSerializer):
    class Meta:
        model = Build
        fields = '__all__'


class FlatAttributeSerializer(serializers.ModelSerializer):
    class Meta:
        model = FlatAttributesValue
        fields = ('name',)


class FlatAttributesValueSerializer(serializers.ModelSerializer):
    name = FlatAttributeSerializer(many=True, read_only=True)

    class Meta:
        model = FlatAttributesValue
        fields = ('id', 'name', 'count', 'description')


class FlatRoomSerializer(serializers.ModelSerializer):
    flat_attributes = FlatAttributesValueSerializer(
        many=True, read_only=True, source='flat_room')

    class Meta:
        model = FlatRoom
        fields = ('id', 'type', 'description', 'flat_attributes')


class BaseFlatsSerializer(serializers.ModelSerializer):
    build = BuildSerializer(read_only=True, source='build_id')
    owner_id = serializers.PrimaryKeyRelatedField(read_only=True)
    flat_rooms = FlatRoomSerializer(many=True, read_only=True, source='room')

    class Meta:
        model = Flat

        fields = (
            'id', 'type', 'room_count', 'owner_id', 'price', 'created_at', 'updated_at',
            'build', 'flat_rooms'
        )


class FlatCreateSerializer(serializers.ModelSerializer):
    build = serializers.PrimaryKeyRelatedField(read_only=True, source='build_id')

    class Meta:
        model = Flat
        fields = ('id', 'type', 'owner_id', 'price', 'build', 'room_count',)
        extra_kwargs = {'flat_rooms': {'read_only': True}}

    def validate_price(self, value):
        if value > 0:
            return value

        raise serializers.ValidationError('The price must be greater than 0 ')

    def validate(self, attrs):
        flat_rooms = self.context.get('flat_rooms')

        for room in flat_rooms:
            flat_attributes = room.get('flat_attributes')

            if len(flat_attributes) < 2:
                raise serializers.ValidationError(
                    'The number of room attributes must be greater than 1.'
                )
            for attr in flat_attributes:
                if attr.get('count') < 1:
                    raise serializers.ValidationError(
                        'The value of the "count" field of the room attribute '
                        'must be greater than 0.'
                    )

        return attrs

    def validate_type(self, value):

        if value in FlatTypes.get_types_list():
            return value

        raise serializers.ValidationError(
            f'The value of the type field must be one of the following: '
            f'{FlatTypes.get_types_list()}.'
        )

    def validate_room_count(self, value):

        if value >= 1:
            return value

        raise serializers.ValidationError('room count < 1')

    def to_internal_value(self, data):

        if 'flat_rooms' not in data:
            raise serializers.ValidationError('key flat_rooms not exist in data')

        self.context['flat_rooms'] = data.pop('flat_rooms')
        data['room_count'] = len(self.context['flat_rooms'])

        return super().to_internal_value(data)

    def create(self, validated_data):
        build_id = self.initial_data.get('build')

        try:
            self.validated_data['build_id'] = Build.objects.get(id=build_id)
        except ObjectDoesNotExist:
            raise serializers.ValidationError('build not exist')

        flat = Flat.objects.create(**self.validated_data)

        for room_data in self.context.get('flat_rooms'):
            room_data['flat_id'] = flat
            room_attr_data = room_data.pop('flat_attributes')
            room_serializer = FlatRoomSerializer(data=room_data)

            if not room_serializer.is_valid():
                raise serializers.ValidationError(room_serializer.errors)

            room = FlatRoom.objects.create(**room_data)

            for flat_attrs_data in room_attr_data:
                flat_attrs_data['flat_room_id'] = room
                attr_idx = flat_attrs_data.pop('id')
                flat_attrs_data['attribute_id'] = FlatAttribute.objects.get(id=attr_idx)
                FlatAttributesValue.objects.create(**flat_attrs_data)

        return {'id': flat.id}


class FlatUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Flat
        fields = ['owner_id', 'price']

    def validate_price(self, value):
        if value > 0:
            return value

        raise serializers.ValidationError('The price must be greater than 0 ')

    def update(self, instance, validated_data):
        instance.owner_id = self.validated_data.get('owner_id', instance.owner_id)
        instance.price = self.validated_data.get('price', instance.price)
        instance.type = self.validated_data.get('type', instance.type)
        instance.save()

        return instance
