from django.db import models
from django.utils import timezone
from flats.config import FlatTypes, RoomTypes
from users.models import CustomUser

FLAT_TYPES = FlatTypes.get_types_choices()
ROOM_TYPES = RoomTypes.get_types_choices()


class Build(models.Model):
    name = models.CharField(max_length=255, verbose_name='build_name')
    address = models.CharField(max_length=255, verbose_name='build_address')
    description = models.TextField(null=False, verbose_name='build_description')

    def __str__(self):
        return str(self.id)


class Flat(models.Model):
    price = models.FloatField(null=False, verbose_name='flat_price')
    room_count = models.IntegerField(null=False, verbose_name='flat_room_count')

    build_id = models.ForeignKey(
        Build, on_delete=models.CASCADE, null=False, related_name='build',
        verbose_name='flat_build'
    )

    owner_id = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, null=False, verbose_name='flat_owner'
    )

    type = models.CharField(max_length=20, choices=FLAT_TYPES, verbose_name='flat_type')

    created_at = models.DateTimeField(
        default=timezone.now, verbose_name='flat_created_at'
    )

    updated_at = models.DateTimeField(
        default=timezone.now, verbose_name='flat_updated_at'
    )

    def __str__(self):
        return str(self.id)


class FlatRoom(models.Model):
    description = models.TextField(null=True, verbose_name='flatroom_description')

    type = models.CharField(
        max_length=255, choices=ROOM_TYPES, verbose_name='flatroom_type'
    )

    flat_id = models.ForeignKey(
        Flat, on_delete=models.CASCADE, null=False, verbose_name='flatroom_flat',
        related_name='room'
    )

    def __str__(self):
        return str(self.id)


class FlatAttribute(models.Model):
    name = models.CharField(max_length=256, verbose_name='flatattribute_name')

    def __str__(self):
        return str(self.id)


class FlatAttributesValue(models.Model):
    description = models.TextField()

    attribute_id = models.ForeignKey(
        FlatAttribute, on_delete=models.CASCADE, null=False,
        verbose_name='flatattributesvalue_attribute'
    )

    flat_room_id = models.ForeignKey(
        FlatRoom, on_delete=models.CASCADE, null=False,
        verbose_name='flatattributesvalue_flat_room', related_name='flat_room',
    )

    count = models.PositiveIntegerField(
        null=False, verbose_name='flatattributesvalue_count'
    )

    created_at = models.DateTimeField(
        default=timezone.now, verbose_name='flat_created_at'
    )

    def __str__(self):
        return str(self.id)
