# Generated by Django 3.2.3 on 2021-05-28 03:04

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Build',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='build_name')),
                ('address', models.CharField(max_length=255, verbose_name='build_address')),
                ('description', models.TextField(verbose_name='build_description')),
            ],
        ),
        migrations.CreateModel(
            name='Flat',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('price', models.FloatField(verbose_name='flat_price')),
                ('room_count', models.IntegerField(verbose_name='flat_room_count')),
                ('type', models.CharField(choices=[('cottage', 'COTTAGE'), ('mansion', 'MANSION'), ('manor', 'MANOR'), ('apartment', 'APARTMENT'), ('house', 'HOUSE'), ('semi_detached_houses', 'SEMI_DETACHED_HOUSES'), ('townhouse', 'TOWNHOUSE'), ('flat', 'FLAT'), ('bungalow', 'BUNGALOW'), ('lodge', 'LODGE'), ('shed', 'SHED'), ('stable', 'STABLE'), ('greenhouse', 'GREENHOUSE'), ('council_house', 'COUNCIL_HOUSE'), ('community_centre', 'COMMUNITY_CENTRE')], max_length=20, verbose_name='flat_type')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='flat_created_at')),
                ('updated_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='flat_updated_at')),
                ('build_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='build', to='flats.build', verbose_name='flat_build')),
            ],
        ),
        migrations.CreateModel(
            name='FlatAttribute',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256, verbose_name='flatattribute_name')),
            ],
        ),
        migrations.CreateModel(
            name='FlatRoom',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.TextField(null=True, verbose_name='flatroom_description')),
                ('type', models.CharField(choices=[('Kitchen', 'KITCHEN'), ('bedroom', 'BEDROOM'), ('bathroom', 'BATHROOM'), ('dining_room', 'DINING_ROOM'), ('living_room', 'LIVING_ROOM'), ('study', 'STUDY'), ('laundry_room', 'LAUNDRY_ROOM'), ('toilet', 'TOILET'), ('shower_room', 'SHOWER_ROOM'), ('box_room', 'BOX_ROOM')], max_length=255, verbose_name='flatroom_type')),
                ('flat_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='room', to='flats.flat', verbose_name='flatroom_flat')),
            ],
        ),
        migrations.CreateModel(
            name='FlatAttributesValue',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.TextField()),
                ('count', models.PositiveIntegerField(verbose_name='flatattributesvalue_count')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='flat_created_at')),
                ('attribute_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='flats.flatattribute', verbose_name='flatattributesvalue_attribute')),
                ('flat_room_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='flat_room', to='flats.flatroom', verbose_name='flatattributesvalue_flat_room')),
            ],
        ),
    ]
