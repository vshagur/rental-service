from django_filters import rest_framework as filters
from flats.models import Flat


class FlatFilter(filters.FilterSet):
    min_price = filters.NumberFilter(field_name="price", lookup_expr='gte')
    max_price = filters.NumberFilter(field_name="price", lookup_expr='lte')
    date_from = filters.DateFilter(field_name="created_at",lookup_expr="gte")
    date_to = filters.DateFilter(field_name="created_at",lookup_expr="lte")

    class Meta:
        model = Flat
        fields = ['min_price', 'max_price', 'created_at', 'date_from', 'date_to',
                  'build_id', 'type', 'created_at']
