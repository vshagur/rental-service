from django.contrib.auth.hashers import make_password
from rest_framework import serializers
from users.config import PHONE_MIN_SIZE, USERNAME_MIN_SIZE, UserStatuses
from users.models import CustomUser


class UserCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = (
            'id', 'username', 'password', 'email', 'phone', 'birthday', 'status',
            'first_name', 'last_name', 'language',
        )
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        """
        Create and return a new CustomUser instance, given the validated data.
        """
        return CustomUser.objects.create(**validated_data)

    def validate_phone(self, value):
        """
        Check phone number.
        """
        if not value.isdigit() or len(value) < PHONE_MIN_SIZE:
            raise serializers.ValidationError(
                f'The phone number cannot be less than {PHONE_MIN_SIZE} digits.')

        return value

    def validate_password(self, value):
        """
        Hash value passed by user.
        """
        return make_password(value)

    def validate_status(self, value):
        """
        Check status field.
        """
        statuses = [i.value for i in UserStatuses]

        if value.lower() not in statuses:
            raise serializers.ValidationError(
                f'User status must be one of the following: {statuses}.')

        return value.lower()

    def validate_username(self, value):
        """
        Check minimal size of username
        """
        if len(value) < USERNAME_MIN_SIZE:
            raise serializers.ValidationError(
                f'Username cannot be shorter than {USERNAME_MIN_SIZE} characters.')

        return value


class UserListSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = (
            'id', 'username', 'email', 'phone', 'birthday', 'status', 'first_name',
            'last_name', 'language', 'age', 'created_at', 'updated_at', 'is_active',
            'is_staff'
        )
