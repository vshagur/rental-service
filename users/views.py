from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics, permissions
from users.config import UserStatuses
from users.models import CustomUser
from users.serializers import UserCreateSerializer, UserListSerializer


class CustomUserCreate(generics.CreateAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = UserCreateSerializer


class CustomUserList(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = UserListSerializer
    filter_backends = [DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    # TODO: add 'age' fields to filterset_fields (does not work now)
    # created vshagur@gmail.com, 2021-05-21
    filterset_fields = ('language',)
    search_fields = ('email', 'phone', 'first_name', 'last_name')
    ordering_fields = ('first_name', 'last_name')

    def get_queryset(self):
        user = self.request.user
        if user.is_staff:
            # staff can view all users
            return CustomUser.objects.all()
        elif user.status == UserStatuses.OWNER.value:
            return CustomUser.objects.filter(status=UserStatuses.RENTER.value)
        elif user.status == UserStatuses.RENTER.value:
            return CustomUser.objects.filter(status=UserStatuses.OWNER.value)
        else:
            raise NotImplementedError


class CustomUserDetail(generics.RetrieveAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = UserListSerializer

    def get_queryset(self):
        user = self.request.user
        if user.is_staff:
            # staff can view all users
            return CustomUser.objects.all()
        elif user.status == 'owner':
            return CustomUser.objects.filter(status='renter')
        elif user.status == 'renter':
            return CustomUser.objects.filter(status='owner')
        else:
            raise NotImplementedError
