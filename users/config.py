from enum import Enum

from django.conf.global_settings import LANGUAGES

USERNAME_MIN_SIZE = 3
PHONE_MIN_SIZE = 4

# WARNING. Changing these settings can lead to database crash and data loss.
# Do not change them unless you are completely sure of what you are doing.
LANGUAGE_CODES = sorted(set([code[:2] for code, value in LANGUAGES]))
LANGUAGE_CODES_CHOICES = [(lang, lang.upper()) for lang in LANGUAGE_CODES]


class UserStatuses(Enum):
    OWNER = 'owner'
    RENTER = 'renter'

    @classmethod
    def get_user_status_choices(cls):
        return [(attr.value, attr.value.upper()) for attr in cls]

    @classmethod
    def get_types_list(cls):
        return [attr.value for attr in cls]
