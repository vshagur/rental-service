from datetime import date

from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
from django.utils import timezone
from users.config import LANGUAGE_CODES_CHOICES, UserStatuses
from users.managers import CustomUserManager

USER_STATUSES = UserStatuses.get_user_status_choices()


class CustomUser(AbstractBaseUser, PermissionsMixin):
    # required fields
    username = models.CharField(
        max_length=150, unique=True, verbose_name='username', null=True
    )

    email = models.EmailField(unique=True, verbose_name='email')
    phone = models.CharField(max_length=13, unique=True, verbose_name='phone')
    birthday = models.DateField(verbose_name='birthday')
    status = models.CharField(max_length=20, choices=USER_STATUSES, verbose_name='status')
    # extra fields
    first_name = models.CharField(
        blank=True, max_length=255, default='', verbose_name='first_name'
    )

    last_name = models.CharField(
        blank=True, max_length=255, default='', verbose_name='last_name'
    )

    language = models.CharField(
        blank=True, max_length=2, choices=LANGUAGE_CODES_CHOICES, verbose_name='language'
    )
    # auto fields
    is_active = models.BooleanField(default=True, verbose_name='is_active')
    is_staff = models.BooleanField(default=False, verbose_name='is_staff')
    created_at = models.DateTimeField(default=timezone.now, verbose_name='created_at')
    updated_at = models.DateTimeField(default=timezone.now, verbose_name='updated_at')

    @property
    def age(self):
        today = date.today()
        age = today.year - self.birthday.year - (
                (today.month, today.day) < (self.birthday.month, self.birthday.day)
        )
        # TODO: why float type? need to find out
        # created vshagur@gmail.com, 2021-05-19
        return float(age)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ('email', 'phone', 'status', 'birthday',)

    objects = CustomUserManager()

    def __str__(self):
        return str(self.id)
