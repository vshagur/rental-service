import datetime

import pytz

from flats.models import Flat
from order.models import Order
from rest_framework import serializers
from users.models import CustomUser


def date_busy_validator(orders, value):
    for order in orders:
        if order.date_from <= value <= order.date_to:
            raise serializers.ValidationError(
                f'You cannot create an order for the date {value}. Dates from '
                f'{order.date_from} to {order.date_to} are already taken.'
            )


def date_in_the_past_validator(value):
    if value < datetime.date.today():
        msg = 'The date cannot be earlier than the current date.'
        raise serializers.ValidationError(msg)


def date_sequence_validator(start_date, end_date):
    if start_date >= end_date:
        raise serializers.ValidationError("The start date is later than the end date.")


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'


class OrderCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ['flat_id', 'date_from', 'date_to']

    def validate_date_from(self, value):
        date_in_the_past_validator(value)
        flat_id = self.initial_data.get('flat_id')
        orders = Order.objects.filter(flat_id=flat_id)
        date_busy_validator(orders, value)

        return value

    def validate_date_to(self, value):
        return self.validate_date_from(value)

    def validate(self, attrs):
        # checking the case when the owner of the flat rents his flat
        user = self.context.get('request').user
        flat_id = self.initial_data.get('flat_id')
        owner = Flat.objects.get(id=flat_id).owner_id

        if user == owner:
            msg = 'The user cannot create an order for his own flat.'
            raise serializers.ValidationError(msg)

        date_sequence_validator(attrs.get('date_from'), attrs.get('date_to'))

        return attrs

    def create(self, validated_data):
        renter = CustomUser.objects.get(id=self.context['request'].user.id)
        validated_data['renter_id'] = renter
        validated_data['created_by'] = renter
        validated_data['updated_by'] = renter
        # calculate and set total_price
        flat = validated_data.get('flat_id')
        date_from = validated_data.get('date_from')
        date_to = validated_data.get('date_to')
        validated_data['total_price'] = round((date_to - date_from).days * flat.price, 2)
        order = Order(**validated_data)
        order.save()
        return {'id': order.id}


class OrderUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ['date_from', 'date_to']

    def validate_date_from(self, value):
        date_in_the_past_validator(value)
        flat_id = self.instance.flat_id
        order_id = self.instance.id
        # compare the new date with the dates of all orders except the current one
        orders = Order.objects.filter(flat_id=flat_id).exclude(id=order_id)
        date_busy_validator(orders, value)

        return value

    def validate_date_to(self, value):
        return self.validate_date_from(value)

    def validate(self, attrs):
        date_sequence_validator(attrs.get('date_from'), attrs.get('date_to'))

        return attrs

    def update(self, instance, validated_data):
        flat = instance.flat_id
        date_from = validated_data.get('date_from')
        date_to = validated_data.get('date_to')
        updated_at = datetime.datetime.now(tz=pytz.UTC)
        total_price = round((date_to - date_from).days * flat.price, 2)

        instance.date_from = date_from
        instance.date_to = date_to
        instance.updated_at = updated_at
        instance.total_price = total_price
        instance.save()

        return {'id': instance.id}
