from django.db import models
from django.utils import timezone
from flats.models import Flat
from users.models import CustomUser


class Order(models.Model):
    renter_id = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, null=False, related_name='order_renter',
        verbose_name='order_renter_id'
    )

    flat_id = models.ForeignKey(
        Flat, on_delete=models.CASCADE, null=False, related_name='order_flat',
        verbose_name='order_flat_id'
    )

    date_from = models.DateField(null=False, verbose_name='order_date_from')
    date_to = models.DateField(null=False, verbose_name='order_date_to')

    created_at = models.DateTimeField(
        default=timezone.now, verbose_name='order_created_at'
    )

    updated_at = models.DateTimeField(
        default=timezone.now, verbose_name='order_updated_at'
    )

    created_by = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, related_name='created_by', null=False,
        verbose_name='order_created_by'
    )

    updated_by = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, null=False, related_name='updated_by',
        verbose_name='order_updated_by'
    )

    total_price = models.FloatField(null=False, verbose_name='order_total_price')

    def __str__(self):
        return str(self.id)
