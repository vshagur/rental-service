from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from order.models import Order
from order.serializers import (OrderCreateSerializer, OrderSerializer,
                               OrderUpdateSerializer)
from rest_framework import filters, generics, permissions, status
from rest_framework.response import Response


class OrderListCreateView(generics.ListCreateAPIView):
    serializer_class = OrderSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    ordering_fields = ('total_price',)

    def get_queryset(self):
        if self.request.method == 'GET':
            return Order.objects.filter(renter_id=self.request.user)
        else:
            return Order.objects.all()

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return OrderSerializer
        else:
            return OrderCreateSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        content = serializer.save()
        return Response(content, status=status.HTTP_201_CREATED)


class OrderRetrieveUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = OrderSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_serializer_class(self):

        if self.request.method == 'PUT':
            return OrderUpdateSerializer

        return self.serializer_class

    def get_queryset(self):

        if self.request.method == 'DELETE' or self.request.method == 'PUT':
            return Order.objects.all()

        return Order.objects.filter(renter_id=self.request.user)

    def update(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        order = get_object_or_404(queryset, id=kwargs.get('pk'))

        if request.user != order.renter_id:
            msg = 'an order can only be deleted by the user who created it'
            return Response({'detail': msg}, status=status.HTTP_403_FORBIDDEN)

        serializer = self.get_serializer(order, data=request.data)
        serializer.is_valid(raise_exception=True)

        try:
            content = serializer.save()
            return Response(content, status=status.HTTP_200_OK)
        except Exception as err:
            return Response({'detail': f'{err}'}, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        order = get_object_or_404(queryset, id=kwargs.get('pk'))

        if request.user != order.renter_id:
            msg = 'an order can only be deleted by the user who created it'
            return Response({'detail': msg}, status=status.HTTP_403_FORBIDDEN)

        order.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)
