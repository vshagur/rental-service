# Generated by Django 3.2.3 on 2021-06-09 05:34

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('flats', '0002_flat_owner_id'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_from', models.DateField(verbose_name='order_date_from')),
                ('date_to', models.DateField(verbose_name='order_date_to')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='order_created_at')),
                ('updated_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='order_updated_at')),
                ('total_price', models.FloatField(verbose_name='order_total_price')),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='created_by', to=settings.AUTH_USER_MODEL, verbose_name='order_created_by')),
                ('flat_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='order_flat', to='flats.flat', verbose_name='order_flat_id')),
                ('renter_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='order_renter', to=settings.AUTH_USER_MODEL, verbose_name='order_renter_id')),
                ('updated_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='updated_by', to=settings.AUTH_USER_MODEL, verbose_name='order_updated_by')),
            ],
        ),
    ]
