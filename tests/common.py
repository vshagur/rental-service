import datetime
from random import choice, choices, randint, sample

import pytz

import factory
from django.contrib.auth import get_user_model
from factory.fuzzy import FuzzyChoice
from faker import Faker
from flats.config import FlatTypes, RoomTypes
from flats.models import Build, Flat, FlatAttribute, FlatRoom
from order.models import Order
from rest_framework.test import APIClient, APITestCase
from rest_framework_simplejwt.tokens import RefreshToken
from users.config import LANGUAGE_CODES, UserStatuses
from users.models import CustomUser


# ======================================================================================
# functions
# ======================================================================================

def generate_user_profile(username=None, email=None, password=None, phone=None,
                          birthday=None, status=None, first_name=None, last_name=None,
                          language=None):
    fake = Faker()

    if username is None:
        username = fake.user_name()

    if password is None:
        password = get_user_model().objects.make_random_password()

    if email is None:
        email = fake.email()

    if phone is None:
        phone = str(Faker().pyint(min_value=1111, max_value=(10 ** 10 - 1), step=1))

    if birthday is None:
        birthday = fake.date_of_birth()

    if status is None:
        status = choice(UserStatuses.get_types_list())

    if first_name is None:
        first_name = fake.first_name()

    if last_name is None:
        last_name = fake.last_name()

    if language is None:
        language = choice(LANGUAGE_CODES)

    return {
        'username': username,
        'password': password,
        'email': email,
        'phone': phone,
        'birthday': birthday,
        'status': status,
        'first_name': first_name,
        'last_name': last_name,
        'language': language,
    }


def generate_user_data(user_profile=None):
    if user_profile is None:
        user_profile = generate_user_profile()

    username = user_profile.pop('username')
    password = user_profile.pop('password')

    return username, password, user_profile


def generator_datetimes(count, start_date, end_date):
    fake = Faker()
    datetimes = [
        fake.date_time_between(
            start_date=start_date, end_date=end_date, tzinfo=pytz.UTC
        )
        for _ in range(count)
    ]

    return datetimes


# ======================================================================================
# factories
# ======================================================================================

class CustomUserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'users.CustomUser'

    username = factory.Faker('user_name')
    email = factory.Faker('email')
    phone = factory.Iterator(set(choices(range(10 ** 4, 10 ** 9), k=100)))
    birthday = factory.Faker('date_of_birth')
    status = FuzzyChoice(UserStatuses.get_types_list())
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    language = FuzzyChoice(LANGUAGE_CODES)
    created_at = FuzzyChoice(generator_datetimes(100, '-100d', '-71d'))
    updated_at = created_at


class BuildFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'flats.Build'

    name = factory.Sequence(lambda n: "Build_%03d" % n)
    address = factory.Iterator(Faker().address() for _ in range(100))
    description = factory.Iterator(Faker().paragraph(nb_sentences=3) for _ in range(100))


class FlatFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'flats.Flat'

    price = FuzzyChoice([randint(10, 1000) + randint(10, 99) / 100 for _ in range(10)])
    room_count = FuzzyChoice([1, 2, 3])
    build_id = factory.SubFactory(BuildFactory)
    owner_id = factory.Iterator(CustomUser.objects.filter(status='owner'))
    type = FuzzyChoice(FlatTypes.get_types_list())
    created_at = FuzzyChoice(generator_datetimes(100, '-70d', '-0d'))
    updated_at = created_at


class FlatRoomFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'flats.FlatRoom'

    description = factory.Iterator(Faker().paragraph(nb_sentences=1) for _ in range(100))
    type = FuzzyChoice(RoomTypes.get_types_list())
    flat_id = factory.Iterator(Flat.objects.all())


class FlatAttributeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'flats.FlatAttribute'

    name = factory.Sequence(lambda n: "FlatAttribute_%03d" % n)


class FlatAttributesValueFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'flats.FlatAttributesValue'

    description = factory.Iterator(Faker().paragraph(nb_sentences=3) for _ in range(100))
    attribute_id = factory.SubFactory(FlatAttributeFactory)
    count = FuzzyChoice([1, 2, 3, 4])
    flat_room_id = factory.SubFactory(FlatRoomFactory)


class OrderFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Order

    renter_id = factory.Iterator(CustomUser.objects.filter(status='renter'))
    created_by = renter_id
    updated_by = renter_id
    created_at = FuzzyChoice(generator_datetimes(100, '-100d', '-71d'))
    updated_at = created_at
    flat_id = factory.Iterator(Flat.objects.all())

    date_to = FuzzyChoice(
        [Faker().date_between(start_date='today', end_date='+30d') for _ in range(100)]
    )

    date_from = FuzzyChoice(
        [Faker().date_between(start_date='-30d', end_date='today') for _ in range(100)]
    )

    # WARNING!!! The generation of this field is random and is not related to
    # the number of days of stay and the price of the flat. Recalculate the cost
    # yourself after generation.
    total_price = FuzzyChoice(
        [round(randint(10, 1000) + randint(10, 99) / 100, 2) for _ in range(10)]
    )


# ======================================================================================
# custom test case classes
# ======================================================================================

class CustomAPITestCase(APITestCase):

    @classmethod
    def create_user(cls, user_data=None):
        User = get_user_model()

        if user_data is None:
            username, password, extra_fields = generate_user_data()
        else:
            username, password, extra_fields = user_data

        return User.objects.create_user(username, password, **extra_fields)

    @classmethod
    def create_auth_client(cls, user=None):

        if user is None:
            user = cls.create_user()

        client = APIClient()
        refresh = RefreshToken.for_user(user)
        client.force_authenticate(user=user, token=refresh.access_token)

        return client

    @classmethod
    def create_anonymous_client(cls):
        return APIClient()

    @classmethod
    def init_users_db(cls, count=20):
        for i in range(count):
            CustomUserFactory()

    def assertLenDictEqualSizeTable(self, count, model_class):
        self.assertEqual(count, model_class.objects.all().count())

    def assertDataEqualDb(self, data, model_class, fields=None):

        if fields is None:
            fields = []

        self.assertIn('id', data)

        idx = data.get('id')
        row = model_class.objects.filter(id=idx).first()
        from django.db import models
        for key in fields:
            self.assertIn(key, data)
            value = getattr(row, key)
            if isinstance(value, models.Model):
                self.assertEqual(data[key], value.id)
            else:
                self.assertEqual(data[key], value)


class DbInitMixin:
    @classmethod
    def init_users_db(cls, count=100, status=None):
        if status is None:
            statuses = choices(UserStatuses.get_types_list(), k=count)
        else:
            statuses = [status for _ in range(count)]

        for status in statuses:
            is_created = False

            while not is_created:
                try:
                    CustomUserFactory(status=status)
                except:
                    continue

                is_created = True

    @classmethod
    def init_build_db(cls, count=20):
        for _ in range(count):
            BuildFactory()

    @classmethod
    def init_flat_db(cls, count=50):
        # init_users_db, init_build_db must be called earlier
        for _ in range(count):
            FlatFactory()

    @classmethod
    def init_flat_room_db(cls):
        # init_users_db, init_build_db, init_flat_db must be called earlier
        for flat in Flat.objects.all():
            for _ in range(flat.room_count):
                FlatRoomFactory(flat_id=flat, )

    @classmethod
    def init_flat_attribute_db(cls, count=10):
        for _ in range(count):
            FlatAttributeFactory()

    @classmethod
    def init_flat_attribute_value_db(cls):
        # init_users_db, init_build_db, init_flat_db, init_flat_attribute_db must
        # be called earlier
        for room in FlatRoom.objects.all():
            attributes = sample(list(FlatAttribute.objects.all()), randint(2, 4))
            for attribute in attributes:
                FlatAttributesValueFactory(
                    flat_room_id=room,
                    attribute_id=attribute,
                    created_at=room.flat_id.created_at
                )

    @classmethod
    def init_order_db(cls):

        users = CustomUser.objects.all()
        flats = Flat.objects.all()
        today = datetime.datetime.now(tz=pytz.UTC)

        for flat in flats:
            date_from = flat.created_at + datetime.timedelta(days=randint(3, 7))
            not_owner_users = users.exclude(id=flat.owner_id.id)

            while (today - date_from).days > 0:
                user = choice(not_owner_users)
                date_to = date_from + datetime.timedelta(days=randint(1, 15))
                date_order = date_from - datetime.timedelta(
                    days=randint(1, (date_from - flat.created_at).days - 1)
                )
                total_price = round(flat.price * (date_to - date_from).days, 2)

                OrderFactory(
                    renter_id=user,
                    flat_id=flat,
                    date_from=date_from,
                    date_to=date_to,
                    created_at=date_order,
                    updated_at=date_order,
                    created_by=user,
                    updated_by=user,
                    total_price=total_price,
                )

                date_from = date_to

    @classmethod
    def init_db(cls):
        cls.init_users_db()
        cls.init_build_db()
        cls.init_flat_db()
        cls.init_flat_room_db()
        cls.init_flat_attribute_db()
        cls.init_flat_attribute_value_db()
        cls.init_order_db()

    @classmethod
    def clean_db(cls):
        # this method for calling in django shell only
        for order in Order.objects.all():
            order.delete()

        for room in FlatRoom.objects.all():
            room.delete()

        for user in CustomUser.objects.all():
            if user.is_superuser:
                continue
            user.delete()

        for build in Build.objects.all():
            build.delete()

        for attr in FlatAttribute.objects.all():
            attr.delete()

        for attr in Order.objects.all():
            attr.delete()
