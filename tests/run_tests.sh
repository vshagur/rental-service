#! /bin/bash

coverage run --source='.'  manage.py test tests.test_users
coverage run --source='.'  manage.py test tests.test_flats
coverage run --source='.'  manage.py test tests.test_order
coverage report
