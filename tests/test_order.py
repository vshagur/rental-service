import datetime
from random import randint

from flats.models import Flat
from order.models import Order
from rest_framework import status
from rest_framework.reverse import reverse
from tests.common import CustomAPITestCase, DbInitMixin
from users.models import CustomUser


class TestOrderApi(CustomAPITestCase, DbInitMixin):
    @classmethod
    def setUpTestData(cls):
        DbInitMixin.init_db()

    @classmethod
    def setUpClass(cls):
        super(TestOrderApi, cls).setUpClass()
        cls.anonymous_client = cls.create_anonymous_client()
        cls.url_orders = reverse('orders')
        cls.user = CustomUser.objects.last()
        cls.auth_client = cls.create_auth_client(user=cls.user)

    def test_get_order_list_return_401_if_unauthorized(self):
        response = self.anonymous_client.get(self.url_orders)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_order_detail_return_401_if_unauthorized(self):
        order_id = Order.objects.last()
        url = reverse('order', args=(order_id,))
        response = self.anonymous_client.get(url)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_order_list_return_correct_data(self):
        user = Order.objects.all().first().renter_id
        auth_client = self.create_auth_client(user=user)
        response = auth_client.get(self.url_orders)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        expected = Order.objects.filter(renter_id=user)
        resp_data = response.json()

        self.assertTrue(isinstance(resp_data, list))
        self.assertEqual(len(resp_data), expected.count())

        # check one only
        data = resp_data[0]
        order = Order.objects.get(id=data.get('id'))
        fields = ['id', 'renter_id', 'flat_id', 'created_by', 'updated_by',
                  'total_price', ]

        self.assertDataEqualDb(data, Order, fields=fields)
        self.assertEqual(data.get('date_from'), order.date_from.isoformat())
        self.assertEqual(data.get('date_to'), order.date_to.isoformat())

        self.assertEqual(
            data.get('created_at')[:19], order.created_at.isoformat()[:19]
        )

        self.assertEqual(
            data.get('updated_at')[:19], order.updated_at.isoformat()[:19]
        )

    def test_get_order_detail_return_correct_data(self):
        order = Order.objects.all().first()
        auth_client = self.create_auth_client(user=order.renter_id)
        url = reverse('order', args=(order.id,))
        response = auth_client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        resp_data = response.json()

        self.assertTrue(isinstance(resp_data, dict))

        fields = ['renter_id', 'flat_id', 'created_by', 'updated_by', 'total_price']
        self.assertDataEqualDb(resp_data, Order, fields)

        self.assertEqual(resp_data.get('date_from'), order.date_from.isoformat())
        self.assertEqual(resp_data.get('date_to'), order.date_to.isoformat())

        # TODO: разобраться, как сравнивать created_at, updated_at без костылей
        #  в виде срезов (есть проблема с timezone)
        # created vshagur@gmail.com, 2021-06-10
        self.assertEqual(
            resp_data.get('created_at')[:19], order.created_at.isoformat()[:19]
        )

        self.assertEqual(
            resp_data.get('updated_at')[:19], order.updated_at.isoformat()[:19]
        )

    def test_get_order_detail_return_404_if_user_is_not_order_creator(self):
        order = Order.objects.all().last()
        user = CustomUser.objects.exclude(id=order.renter_id.id).first()
        auth_client = self.create_auth_client(user=user)
        url = reverse('order', args=(order.id,))
        response = auth_client.get(url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create_order(self):
        orders = Order.objects.all()
        count = orders.count()
        order = orders.last()
        user = CustomUser.objects.exclude(id=order.renter_id.id).last()
        auth_client = self.create_auth_client(user=user)
        date_from = order.date_to + datetime.timedelta(days=3)
        date_to = order.date_to + datetime.timedelta(days=randint(4, 14))
        flat = order.flat_id
        data = {'flat_id': flat.id, 'date_from': date_from, 'date_to': date_to}
        response = auth_client.post(self.url_orders, data=data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Order.objects.all().count(), count + 1)

        resp_data = response.json()

        self.assertIn('id', resp_data)

        try:
            new_order = Order.objects.get(id=resp_data.get('id'))
        except:
            raise AssertionError('data not save into db')

        self.assertEqual(new_order.id, resp_data.get('id'))
        self.assertEqual(new_order.created_by, user)
        self.assertEqual(new_order.updated_by, user)
        self.assertEqual(new_order.flat_id, flat)
        self.assertEqual(new_order.renter_id, user)
        self.assertEqual(new_order.date_to, date_to)
        self.assertEqual(new_order.date_from, date_from)

        total_price = round((date_to - date_from).days * flat.price, 2)
        self.assertEqual(new_order.total_price, total_price)

    def test_not_create_order_if_send_not_valid_data(self):
        orders = Order.objects.all()
        count = orders.count()
        order = orders.last()
        user = CustomUser.objects.exclude(id=order.renter_id.id).last()
        auth_client = self.create_auth_client(user=user)
        flat = order.flat_id

        # checking the case when the date_from > date_to
        date_to = order.date_to + datetime.timedelta(days=10)
        date_from = order.date_to + datetime.timedelta(days=20)
        data = {'flat_id': flat.id, 'date_from': date_from, 'date_to': date_to}
        response = auth_client.post(self.url_orders, data=data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Order.objects.all().count(), count)

        # checking the case when the date is busy
        date_from = order.date_from + datetime.timedelta(days=1)
        date_to = order.date_to + datetime.timedelta(days=randint(4, 14))
        data = {'flat_id': flat.id, 'date_from': date_from, 'date_to': date_to}
        response = auth_client.post(self.url_orders, data=data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Order.objects.all().count(), count)

        # checking the case when a required parameter is not passed
        date_from = order.date_to + datetime.timedelta(days=3)
        data = {'date_from': date_from, 'date_to': date_to}
        response = auth_client.post(self.url_orders, data=data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Order.objects.all().count(), count)

        # checking the case when the owner of the flat rents his flat
        data = {'flat_id': flat.id, 'date_from': date_from, 'date_to': date_to}
        auth_client = self.create_auth_client(user=flat.owner_id)
        response = auth_client.post(self.url_orders, data=data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Order.objects.all().count(), count)

    def test_delete_order(self):
        orders = Order.objects.all()
        order = orders.last()
        count = orders.count()
        order_id = order.id
        user = order.renter_id
        auth_client = self.create_auth_client(user=user)
        url = reverse('order', args=(order_id,))
        response = auth_client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Order.objects.all().count(), count - 1)
        self.assertFalse(Order.objects.filter(id=order_id).exists())

    def test_delete_order_return_403_if_user_is_not_order_creator(self):
        orders = Order.objects.all()
        order = orders.first()
        count = orders.count()
        order_id = order.id
        user = CustomUser.objects.exclude(id=order.renter_id.id).first()
        auth_client = self.create_auth_client(user=user)
        url = reverse('order', args=(order_id,))
        response = auth_client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(Order.objects.all().count(), count)

    def test_update_order_when_new_dates_not_intersect_with_current(self):
        """checking the case when new dates do not intersect with the current ones"""

        orders = Order.objects.all()
        count = orders.count()
        order = orders.last()
        user = order.renter_id
        order_id = order.id
        updated_at = order.updated_at
        auth_client = self.create_auth_client(user=user)
        flat = order.flat_id
        url = reverse('order', args=(order_id,))
        date_from = order.date_from + datetime.timedelta(days=50)
        date_to = order.date_to + datetime.timedelta(days=70)
        data = {'date_from': date_from, 'date_to': date_to}
        response = auth_client.put(url, data=data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Order.objects.all().count(), count)
        self.assertTrue(Order.objects.filter(id=order_id).exists())

        updated_order = Order.objects.get(id=order_id)

        self.assertEqual(updated_order.date_to, date_to)
        self.assertEqual(updated_order.date_from, date_from)
        self.assertEqual(updated_order.flat_id, flat)
        self.assertEqual(updated_order.renter_id, user)
        self.assertEqual(updated_order.updated_by, user)
        self.assertEqual(
            updated_order.total_price, round((date_to - date_from).days * flat.price, 2)
        )
        self.assertTrue(updated_order.updated_at > updated_at)

        resp_data = response.json()
        self.assertIn('id', resp_data)
        self.assertEqual(resp_data.get('id'), order_id)

    def test_update_order_when_new_dates_intersect_with_current(self):
        """checking the case when new dates do intersect with the current ones"""

        orders = Order.objects.filter(date_to__gt=datetime.date.today())
        count = Order.objects.all().count()
        order = orders.last()
        user = order.renter_id
        order_id = order.id
        updated_at = order.updated_at
        auth_client = self.create_auth_client(user=user)
        flat = order.flat_id
        url = reverse('order', args=(order_id,))
        date_from = datetime.date.today() + datetime.timedelta(days=1)
        date_to = datetime.date.today() + datetime.timedelta(days=3)
        data = {'date_from': date_from, 'date_to': date_to}
        response = auth_client.put(url, data=data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Order.objects.all().count(), count)
        self.assertTrue(Order.objects.filter(id=order_id).exists())

        updated_order = Order.objects.get(id=order_id)

        self.assertEqual(updated_order.date_to, date_to)
        self.assertEqual(updated_order.date_from, date_from)
        self.assertEqual(updated_order.flat_id, flat)
        self.assertEqual(updated_order.renter_id, user)
        self.assertEqual(updated_order.updated_by, user)
        self.assertEqual(
            updated_order.total_price, round((date_to - date_from).days * flat.price, 2)
        )
        self.assertTrue(updated_order.updated_at > updated_at)

        resp_data = response.json()
        self.assertIn('id', resp_data)
        self.assertEqual(resp_data.get('id'), order_id)

    def test_get_orders_return_data_ordering_by_total_price(self):
        current_count, flat_id = 0, None

        for flat in Flat.objects.all():
            count = Order.objects.filter(flat_id=flat.id).count()
            if count > current_count:
                flat_id = flat.id
                current_count = count

        orders = Order.objects.filter(flat_id=flat_id)
        user = orders.first().renter_id
        auth_client = self.create_auth_client(user=user)
        url = reverse('orders') + f'?ordering=total_price'
        response = auth_client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        resp_data = response.json()
        price_fied_values = [item['total_price'] for item in resp_data]

        self.assertEqual(price_fied_values, sorted(price_fied_values))

        url = reverse('orders') + f'?ordering=-total_price'
        response = auth_client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        resp_data = response.json()
        price_fied_values = [item['total_price'] for item in resp_data]

        self.assertEqual(price_fied_values, sorted(price_fied_values, reverse=True))

    def test_not_update_order_if_user_send_not_valid_data(self):
        orders = Order.objects.all()
        count = orders.count()
        order = orders.last()
        user = order.renter_id
        order_id = order.id
        auth_client = self.create_auth_client(user=user)
        date_from = order.date_from
        date_to = order.date_to

        # checking the case when the date_from > date_to
        new_date_from = order.date_from + datetime.timedelta(days=50)
        new_date_to = order.date_to + datetime.timedelta(days=70)
        data = {'date_from': new_date_to, 'date_to': new_date_from}
        url = reverse('order', args=(order_id,))
        response = auth_client.put(url, data=data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Order.objects.all().count(), count)

        try:
            new_order = Order.objects.get(id=order_id)
        except:
            raise AssertionError('data not save into db')

        self.assertEqual(new_order.date_to, date_to)
        self.assertEqual(new_order.date_from, date_from)

        # checking the case when a non-existing order id was sent
        new_date_from = order.date_from + datetime.timedelta(days=50)
        new_date_to = order.date_to + datetime.timedelta(days=70)
        data = {'date_from': new_date_from, 'date_to': new_date_to}
        url = reverse('order', args=(999999999,))
        response = auth_client.put(url, data=data, format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(Order.objects.all().count(), count)

        try:
            new_order = Order.objects.get(id=order_id)
        except:
            raise AssertionError('data not save into db')

        self.assertEqual(new_order.date_to, date_to)
        self.assertEqual(new_order.date_from, date_from)

        # checking the case when the date was sent earlier than the current one
        new_date_from = datetime.date.today() + datetime.timedelta(days=-1)
        new_date_to = order.date_to + datetime.timedelta(days=70)
        data = {'date_from': new_date_from, 'date_to': new_date_to}
        url = reverse('order', args=(order_id,))
        response = auth_client.put(url, data=data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Order.objects.all().count(), count)

        try:
            new_order = Order.objects.get(id=order_id)
        except:
            raise AssertionError('data not save into db')

        self.assertEqual(new_order.date_to, date_to)
        self.assertEqual(new_order.date_from, date_from)
