from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from tests.common import (CustomAPITestCase, generate_user_data,
                          generate_user_profile)
from users.models import CustomUser


class UsersCreateTests(TestCase):

    def setUp(self):
        self.username, self.password, self.extra_fields = generate_user_data()

    def test_create_superuser(self):
        username = 'admin'
        User = get_user_model()
        user = User.objects.create_superuser(username, self.password, **self.extra_fields)

        self.assertEqual(user.username, username)
        self.assertEqual(user.email, self.extra_fields.get('email'))
        self.assertEqual(user.phone, self.extra_fields.get('phone'))
        self.assertEqual(user.status, self.extra_fields.get('status'))
        self.assertEqual(user.birthday, self.extra_fields.get('birthday'))
        self.assertTrue(user.is_active)
        self.assertTrue(user.is_staff)
        self.assertTrue(user.is_superuser)

    def test_create_user(self):
        User = get_user_model()
        user = User.objects.create_user(self.username, self.password, **self.extra_fields)

        self.assertEqual(user.username, self.username)
        self.assertEqual(user.email, self.extra_fields.get('email'))
        self.assertEqual(user.phone, self.extra_fields.get('phone'))
        self.assertEqual(user.status, self.extra_fields.get('status'))
        self.assertEqual(user.birthday, self.extra_fields.get('birthday'))
        self.assertEqual(user.first_name, self.extra_fields.get('first_name'))
        self.assertEqual(user.last_name, self.extra_fields.get('last_name'))
        self.assertEqual(user.language, self.extra_fields.get('language'))
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)


class TestRegisterApi(CustomAPITestCase):

    def setUp(self):
        self.anonymous_client = self.create_anonymous_client()
        self.profile = generate_user_profile()
        self.url = reverse('register')

    def test_create_user_request_return_200(self):
        response = self.anonymous_client.post(self.url, data=self.profile)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_user_request_write_correct_data_to_db(self):
        self.anonymous_client.post(self.url, data=self.profile)

        self.assertEqual(CustomUser.objects.count(), 1)

        user = CustomUser.objects.first()

        self.assertEqual(user.username, self.profile.get('username'))
        self.assertEqual(user.email, self.profile.get('email'))
        self.assertEqual(user.phone, self.profile.get('phone'))
        self.assertEqual(user.status, self.profile.get('status'))
        self.assertEqual(user.birthday, self.profile.get('birthday'))
        self.assertEqual(user.first_name, self.profile.get('first_name'))
        self.assertEqual(user.last_name, self.profile.get('last_name'))
        self.assertEqual(user.language, self.profile.get('language'))
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)
        self.assertTrue(user.is_active)

    def test_create_user_request_return_400_if_send_too_lohg_username(self):
        self.profile['username'] = 'a' * 151
        response = self.anonymous_client.post(self.url, data=self.profile)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_user_request_return_400_if_send_too_short_username(self):
        self.profile['username'] = 'aa'
        response = self.anonymous_client.post(self.url, data=self.profile)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_user_request_return_400_if_send_not_valid_email(self):
        self.profile['email'] = 'not@valid@email'
        response = self.anonymous_client.post(self.url, data=self.profile)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_user_request_return_400_if_send_not_valid_phone(self):
        self.profile['phone'] = '123-456-78'
        response = self.anonymous_client.post(self.url, data=self.profile)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_user_request_return_400_if_send_not_valid_status(self):
        self.profile['status'] = 'admin'
        response = self.anonymous_client.post(self.url, data=self.profile)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TestUsersListApi(CustomAPITestCase):

    @classmethod
    def setUpClass(cls):
        super(TestUsersListApi, cls).setUpClass()
        cls.url = reverse('users')
        cls.init_users_db()
        cls.renters = CustomUser.objects.filter(status='renter')
        cls.owners = CustomUser.objects.filter(status='owner')

    def test_get_users_list_return_401_if_unauthorized(self):
        anonymous_client = self.create_anonymous_client()
        response = anonymous_client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_users_list_return_200(self):
        for user in (self.renters.last(), self.owners.last()):
            with self.subTest(user=user):
                auth_client = self.create_auth_client(user=user)
                response = auth_client.get(self.url)
                self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_users_list_return_only_owners_if_user_is_renter(self):
        auth_client = self.create_auth_client(user=self.renters.last())
        data = auth_client.get(self.url).json()

        self.assertEqual(len(data), self.owners.count())

        check_list = filter(lambda obj: obj['status'] == 'owner', data)
        self.assertTrue(all(list(check_list)))

    def test_get_users_list_return_only_renters_if_user_is_owner(self):
        auth_client = self.create_auth_client(user=self.owners.last())
        data = auth_client.get(self.url).json()

        self.assertEqual(len(data), self.renters.count())

        check_list = filter(lambda obj: obj['status'] == 'renter', data)
        self.assertTrue(all(list(check_list)))


class TestUsersDetailApi(CustomAPITestCase):
    @classmethod
    def setUpClass(cls):
        super(TestUsersDetailApi, cls).setUpClass()
        cls.url = reverse('user', kwargs={'pk': 1})
        cls.init_users_db()

        cls.renters = CustomUser.objects.filter(status='renter')
        cls.owners = CustomUser.objects.filter(status='owner')

    def test_get_user_detail_return_401_if_unauthorized(self):
        anonymous_client = self.create_anonymous_client()
        response = anonymous_client.get(self.url)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_user_detail_return_404_if_users_status_equals(self):
        for users in (self.renters, self.owners):
            with self.subTest(users=users):
                auth_client = self.create_auth_client(user=users.first())
                user_id = users.last().pk
                response = auth_client.get(reverse('user', kwargs={'pk': user_id}))

                self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_user_detail_return_correct_response_if_user_owner(self):
        auth_client = self.create_auth_client(user=self.owners.first())
        user_id = self.renters.last().pk
        response = auth_client.get(reverse('user', kwargs={'pk': user_id}))
        data = response.json()
        expected_user = CustomUser.objects.get(id=user_id)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(data.get('id'), expected_user.pk)
        self.assertEqual(data.get('email'), expected_user.email)
        self.assertEqual(data.get('username'), expected_user.username)
        self.assertEqual(data.get('phone'), expected_user.phone)
        self.assertEqual(data.get('birthday'), expected_user.birthday.isoformat())
        self.assertEqual(data.get('status'), expected_user.status)
        self.assertEqual(data.get('first_name'), expected_user.first_name)
        self.assertEqual(data.get('last_name'), expected_user.last_name)
        self.assertEqual(data.get('language'), expected_user.language)
        self.assertEqual(data.get('is_active'), expected_user.is_active)

    def test_get_user_detail_return_correct_response_if_user_renter(self):
        auth_client = self.create_auth_client(user=self.renters.first())
        user_id = self.owners.last().pk
        response = auth_client.get(reverse('user', kwargs={'pk': user_id}))
        data = response.json()
        expected_user = CustomUser.objects.get(id=user_id)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(data.get('id'), expected_user.pk)
        self.assertEqual(data.get('email'), expected_user.email)
        self.assertEqual(data.get('username'), expected_user.username)
        self.assertEqual(data.get('phone'), expected_user.phone)
        self.assertEqual(data.get('birthday'), expected_user.birthday.isoformat())
        self.assertEqual(data.get('status'), expected_user.status)
        self.assertEqual(data.get('first_name'), expected_user.first_name)
        self.assertEqual(data.get('last_name'), expected_user.last_name)
        self.assertEqual(data.get('language'), expected_user.language)
        self.assertEqual(data.get('is_active'), expected_user.is_active)
