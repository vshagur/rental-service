from random import choice

from django.db.models import Avg
from flats.models import (Build, Flat, FlatAttribute, FlatAttributesValue,
                          FlatRoom)
from rest_framework import status
from rest_framework.reverse import reverse
from tests.common import CustomAPITestCase, DbInitMixin
from users.models import CustomUser

FLATS_FIELDS = (
    'id', 'type', 'price', 'room_count', 'created_at', 'updated_at', 'owner_id',
    'flat_rooms', 'build'
)


class TestFlatsApi(CustomAPITestCase, DbInitMixin):
    @classmethod
    def setUpTestData(cls):
        DbInitMixin.init_db()

    @classmethod
    def setUpClass(cls):
        super(TestFlatsApi, cls).setUpClass()
        cls.url = reverse('flats')
        cls.owners = CustomUser.objects.filter(status='owner')
        cls.user = cls.owners.last()
        cls.auth_client = cls.create_auth_client(user=cls.owners.last())
        cls.all_flats_data = Flat.objects.all()
        cls.detail_url = reverse('flat', args=(cls.all_flats_data.last(),))

    def test_get_flats_list_return_401_if_unauthorized(self):
        anonymous_client = self.create_anonymous_client()
        response = anonymous_client.get(self.url)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_flats_list_return_200(self):
        resp = self.auth_client.get(self.url)

        self.assertEqual(resp.status_code, status.HTTP_200_OK)

    def test_get_flats_list_return_correct_flats_data(self):
        resp = self.auth_client.get(self.url)

        self.assertLenDictEqualSizeTable(len(resp.json()), Flat)

        for flat_data in resp.json():

            # check format
            for field in FLATS_FIELDS:
                self.assertIn(field, flat_data)

            # check data
            self.assertDataEqualDb(
                flat_data, Flat, fields=['type', 'price', 'room_count']
            )

            self.assertDataEqualDb(flat_data['build'], Build, fields=['name', 'address'])

            # check one only
            flat_room_data = flat_data.get('flat_rooms')[-1]

            self.assertIn('flat_attributes', flat_room_data)

            self.assertDataEqualDb(
                flat_room_data, FlatRoom, fields=['type', 'description', ]
            )

            # check one only
            flat_attributes_data = flat_room_data.get('flat_attributes')[-1]

            self.assertDataEqualDb(
                flat_attributes_data, FlatAttributesValue,
                fields=['count', 'description', ]
            )

    def test_get_flats_detail_return_401_if_unauthorized(self):
        anonymous_client = self.create_anonymous_client()
        response = anonymous_client.get(self.detail_url)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_flats_detail_return_200(self):
        resp = self.auth_client.get(self.detail_url)

        self.assertEqual(resp.status_code, status.HTTP_200_OK)

    def test_get_flats_detail_return_correct_flats_data(self):
        flat_data = self.auth_client.get(self.detail_url).json()

        for field in FLATS_FIELDS:
            self.assertIn(field, flat_data)

        # check data
        self.assertDataEqualDb(
            flat_data, Flat, fields=['type', 'price', 'room_count']
        )

        self.assertDataEqualDb(
            flat_data['build'], Build, fields=['name', 'address']
        )

        # check one only
        flat_room_data = flat_data.get('flat_rooms')[-1]

        self.assertIn('flat_attributes', flat_room_data)

        self.assertDataEqualDb(
            flat_room_data, FlatRoom, fields=['type', 'description', ]
        )

        # check one only
        flat_attributes_data = flat_room_data.get('flat_attributes')[-1]

        self.assertDataEqualDb(
            flat_attributes_data, FlatAttributesValue,
            fields=['count', 'description', ]
        )

    def test_create_flat(self):
        # TODO: разобраться почему криво работают setUpTestData и setUpClass
        # при выносе этого теста в отдельный класс
        # created vshagur@gmail.com, 2021-06-3
        self.url = reverse('flats')
        expected_count = Flat.objects.all().count() + 1
        expected_data = {
            "type": "flat",
            "owner_id": self.user.id,
            "price": 429.25,
            "build": Build.objects.all().last().id,
            "flat_rooms": [
                {
                    "type": "dining_room",
                    "description": "Peace per close car stuff next.",
                    "flat_attributes": [
                        {
                            "id": FlatAttribute.objects.all().first().id,
                            "count": 2,
                        },
                        {
                            "id": FlatAttribute.objects.all().last().id,
                            "count": 1,
                        },
                    ]
                },

                {
                    "type": "bedroom",
                    "description": "Peace per.",
                    "flat_attributes": [
                        {
                            "id": FlatAttribute.objects.all().first().id,
                            "count": 3,
                        },
                        {
                            "id": FlatAttribute.objects.all().last().id,
                            "count": 2,
                        },
                    ]
                },
            ]
        }

        resp = self.auth_client.post(self.url, data=expected_data, format='json')

        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Flat.objects.all().count(), expected_count)

        data = resp.json()
        self.assertIn('id', data.keys())

        flat = Flat.objects.get(id=data.get('id'))
        self.assertEqual(flat.type, expected_data.get('type'))
        self.assertEqual(flat.owner_id_id, expected_data.get('owner_id'))
        self.assertEqual(flat.build_id_id, expected_data.get('build'))
        self.assertEqual(flat.price, expected_data.get('price'))
        self.assertEqual(flat.room_count, len(expected_data.get('flat_rooms')))

        rooms = FlatRoom.objects.filter(flat_id=data.get('id'))
        self.assertEqual(len(list(rooms)), len(expected_data.get('flat_rooms')))

        room = rooms.first()
        expected_room = expected_data['flat_rooms'][0]  # check one only
        self.assertEqual(room.type, expected_room['type'])
        self.assertEqual(room.description, expected_room['description'])

        room_attributes = FlatAttributesValue.objects.filter(flat_room_id=room.id)
        self.assertEqual(len(list(room_attributes)), 2)

    def test_not_create_flat_if_room_attributes_less_then_2(self):
        self.url = reverse('flats')
        expected_count = Flat.objects.all().count()
        expected_data = {
            "type": "flat",
            "owner_id": self.user.id,
            "price": 429.25,
            "build": Build.objects.all().last().id,
            "flat_rooms": [
                {
                    "type": "dining_room",
                    "description": "Peace per close car stuff next.",
                    "flat_attributes": [
                        {
                            "id": FlatAttribute.objects.all().first().id,
                            "count": 2,
                        },
                        {
                            "id": FlatAttribute.objects.all().last().id,
                            "count": 1,
                        },
                    ]
                },

                {
                    "type": "bedroom",
                    "description": "Peace per.",
                    "flat_attributes": [
                        {
                            "id": FlatAttribute.objects.all().first().id,
                            "count": 3,
                        },
                    ]
                },
            ]
        }

        resp = self.auth_client.post(self.url, data=expected_data, format='json')

        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Flat.objects.all().count(), expected_count)

    def test_not_create_flat_if_value_of_field_count_of_flat_attributes_less_than_1(self):
        self.url = reverse('flats')
        expected_count = Flat.objects.all().count()
        expected_data = {
            "type": "flat",
            "owner_id": self.user.id,
            "price": 429.25,
            "build": Build.objects.all().last().id,
            "flat_rooms": [
                {
                    "type": "dining_room",
                    "description": "Peace per close car stuff next.",
                    "flat_attributes": [
                        {
                            "id": FlatAttribute.objects.all().first().id,
                            "count": 2,
                        },
                        {
                            "id": FlatAttribute.objects.all().last().id,
                            "count": 1,
                        },
                    ]
                },

                {
                    "type": "bedroom",
                    "description": "Peace per.",
                    "flat_attributes": [
                        {
                            "id": FlatAttribute.objects.all().first().id,
                            "count": 3,
                        },
                        {
                            "id": FlatAttribute.objects.all().last().id,
                            "count": 0,  # error here
                        },
                    ]
                },
            ]
        }

        resp = self.auth_client.post(self.url, data=expected_data, format='json')

        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Flat.objects.all().count(), expected_count)

    def test_update_flat(self):
        expected_flats_count = Flat.objects.all().count()
        flat = Flat.objects.last()
        owner = flat.owner_id
        new_price = flat.price + 100
        url = reverse('flat', args=(flat.id,))
        new_owner = CustomUser.objects.filter(status='owner').exclude(pk=owner.id).last()

        expected_data = {
            "owner_id": new_owner.id,
            "price": new_price,
        }

        auth_client = self.create_auth_client(user=owner)
        resp = auth_client.put(url, data=expected_data, format='json')

        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(Flat.objects.all().count(), expected_flats_count)

        updated_flat = Flat.objects.get(pk=flat.id)
        self.assertEqual(updated_flat.owner_id, new_owner)
        self.assertEqual(updated_flat.price, new_price)

    def test_delete_flat(self):
        flat = Flat.objects.all().last()
        user = flat.owner_id
        auth_client = self.create_auth_client(user)
        url = reverse('flat', args=(flat.id,))
        resp = auth_client.delete(url, format='json')

        self.assertEqual(resp.status_code, status.HTTP_204_NO_CONTENT)

        flat = Flat.objects.filter(pk=flat.id)
        self.assertFalse(flat.exists())

    def test_not_owner_does_not_delete_flat(self):
        flat = Flat.objects.all().last()
        owner = flat.owner_id
        user = CustomUser.objects.filter(status='owner').exclude(pk=owner.id).last()
        auth_client = self.create_auth_client(user)
        url = reverse('flat', args=(flat.id,))
        resp = auth_client.delete(url, format='json')

        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)

        flat = Flat.objects.filter(pk=flat.id)
        self.assertTrue(flat.exists())

    def test_get_flats_list_return_sorted_data_by_owner_id(self):
        url = reverse('flats') + f'?ordering=owner_id'
        resp = self.auth_client.get(url)

        self.assertEqual(resp.status_code, status.HTTP_200_OK)

        owner_id_list = [flat['owner_id'] for flat in resp.json()]

        self.assertEqual(owner_id_list, sorted(owner_id_list))

    def test_get_flats_list_return_sorted_data_by_room_count(self):
        url = reverse('flats') + f'?ordering=room_count'
        resp = self.auth_client.get(url)

        self.assertEqual(resp.status_code, status.HTTP_200_OK)

        room_count_list = [flat['room_count'] for flat in resp.json()]

        self.assertEqual(room_count_list, sorted(room_count_list))

    def test_get_flats_list_return_sorted_data_by_build_id(self):
        url = reverse('flats') + f'?ordering=build_id'
        resp = self.auth_client.get(url)

        self.assertEqual(resp.status_code, status.HTTP_200_OK)

        build_id_list = [flat['build']['id'] for flat in resp.json()]

        self.assertEqual(build_id_list, sorted(build_id_list))

    def test_get_flats_list_return_sorted_data_by_owner_id_reverse(self):
        url = reverse('flats') + f'?ordering=-owner_id'
        resp = self.auth_client.get(url)

        self.assertEqual(resp.status_code, status.HTTP_200_OK)

        owner_id_list = [flat['owner_id'] for flat in resp.json()]

        self.assertEqual(owner_id_list, sorted(owner_id_list, reverse=True))

    def test_get_flats_list_return_sorted_data_by_room_count_reverse(self):
        url = reverse('flats') + f'?ordering=-room_count'
        resp = self.auth_client.get(url)

        self.assertEqual(resp.status_code, status.HTTP_200_OK)

        room_count_list = [flat['room_count'] for flat in resp.json()]

        self.assertEqual(room_count_list, sorted(room_count_list, reverse=True))

    def test_get_flats_list_return_sorted_data_by_build_id_reverse(self):
        url = reverse('flats') + f'?ordering=-build_id'
        resp = self.auth_client.get(url)

        self.assertEqual(resp.status_code, status.HTTP_200_OK)

        build_id_list = [flat['build']['id'] for flat in resp.json()]

        self.assertEqual(build_id_list, sorted(build_id_list, reverse=True))

    def test_get_flats_list_return_filtered_data_by_type(self):
        flat = choice(list(Flat.objects.all()))
        expected = [item.id for item in Flat.objects.filter(type=flat.type)]
        url = reverse('flats') + f'?type={flat.type}'
        resp = self.auth_client.get(url)

        self.assertEqual(resp.status_code, status.HTTP_200_OK)

        flat_id_list = [flat['id'] for flat in resp.json()]

        self.assertEqual(flat_id_list, expected)

    def test_get_flats_list_return_filtered_data_by_price(self):
        avg_price = int(Flat.objects.all().aggregate(Avg('price')).get('price__avg'))
        url = reverse('flats') + f'?min_price={avg_price}'
        expected = [flat.id for flat in Flat.objects.filter(price__gte=avg_price)]
        resp = self.auth_client.get(url)

        self.assertEqual(resp.status_code, status.HTTP_200_OK)

        flat_id_list = [flat['id'] for flat in resp.json()]

        self.assertEqual(flat_id_list, expected)

    def test_get_flats_list_return_filtered_data_by_date(self):
        # TODO: disable warnings into this tests
        # created vshagur@gmail.com, 2021-06-6
        dates = sorted(set([flat.created_at.date() for flat in Flat.objects.all()]))
        date_from, date_to = dates[1], dates[-2]
        url = reverse('flats') + f'?date_from={date_from}&date_to={date_to}'
        resp = self.auth_client.get(url)

        self.assertEqual(resp.status_code, status.HTTP_200_OK)

        data = resp.json()
        expected = Flat.objects.filter(
            created_at__gte=date_from).filter(created_at__lte=date_to)

        self.assertEqual(len(data), len(expected))

        flat_ids = [item['id'] for item in data]
        expected_flat_ids = [item.id for item in expected]

        self.assertEqual(flat_ids, expected_flat_ids)

    def test_get_flats_list_return_filtered_data_by_build_id(self):
        flat = choice(list(Flat.objects.all()))
        expected = [item.id for item in Flat.objects.filter(build_id=flat.build_id)]
        url = reverse('flats') + f'?build_id={flat.build_id}'
        resp = self.auth_client.get(url)

        self.assertEqual(resp.status_code, status.HTTP_200_OK)

        flat_id_list = [flat['id'] for flat in resp.json()]

        self.assertEqual(flat_id_list, expected)

    def test_get_room_detail_return_correct_data(self):
        room = FlatRoom.objects.all().last()
        url = reverse('room', args=(room.flat_id, room))
        resp = self.auth_client.get(url, format='json')

        self.assertEqual(resp.status_code, status.HTTP_200_OK)

        resp_data = resp.json()

        self.assertEqual(resp_data.get('id'), room.id)
        self.assertEqual(resp_data.get('description'), room.description)
        self.assertEqual(resp_data.get('type'), room.type)

        flat_attributes = resp_data.get('flat_attributes')
        expected = FlatAttributesValue.objects.filter(flat_room_id=room)

        self.assertTrue(isinstance(flat_attributes, list))
        self.assertEqual(len(flat_attributes), len(list(expected)))
        # check one only
        self.assertEqual(flat_attributes[0].get('id'), expected.first().id)
        self.assertEqual(flat_attributes[0].get('count'), expected.first().count)
        self.assertEqual(
            flat_attributes[0].get('description'), expected.first().description)

    def test_get_room_detail_return_401_if_unauthorized(self):
        room = FlatRoom.objects.all().last()
        url = reverse('room', args=(room.flat_id, room))
        client = self.create_anonymous_client()
        resp = client.get(url, format='json')

        self.assertEqual(resp.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_rooms_list_return_correct_data(self):
        flat = Flat.objects.filter(room_count__gte=2).first()
        url = reverse('rooms', args=(flat.id,))
        resp = self.auth_client.get(url, format='json')

        self.assertEqual(resp.status_code, status.HTTP_200_OK)

        resp_data = resp.json()

        self.assertTrue(isinstance(resp_data, list))
        self.assertEqual(len(resp_data), FlatRoom.objects.filter(flat_id=flat).count())

        # check room id only
        expected_ids = [room.id for room in FlatRoom.objects.filter(flat_id=flat)]
        resp_ids = [room.get('id') for room in resp_data]

        self.assertEqual(resp_ids, expected_ids)

    def test_get_rooms_list_return_401_if_unauthorized(self):
        flat = Flat.objects.filter(room_count__gte=2).first()
        url = reverse('rooms', args=(flat.id,))
        client = self.create_anonymous_client()
        resp = client.get(url, format='json')

        self.assertEqual(resp.status_code, status.HTTP_401_UNAUTHORIZED)
